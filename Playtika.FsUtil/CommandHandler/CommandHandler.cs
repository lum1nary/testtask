﻿using System;
using System.Collections.Concurrent;
using System.IO;
using System.Threading.Tasks;
using Autofac;
using Autofac.Core.Registration;
using log4net;

namespace Playtika.FsUtil
{
    public class CommandHandler
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(CommandHandler));
        public ICommandAction Action { get; }
        private ConcurrentBag<string[]> _result;

        public CommandHandler(ICommandActionSource actionSource)
        {
            if (actionSource == null)
                throw new ArgumentNullException(nameof(actionSource));

            try
            {
                var actionParam = new TypedParameter(typeof(ICommandActionSource), actionSource);
                Action = SvcProvider.Instance.ResolveNamed<ICommandAction>(actionSource.CommandAction, actionParam);
            }
            catch (ComponentNotRegisteredException)
            {
                Log.Error($"command {actionSource.CommandAction} not found");
                throw;
            }
        }

        public async Task<string[][]> Handle()
        {
            _result = new ConcurrentBag<string[]>();
            await HandleInternal(Action.ActionSource.SourceDir);
            return await Task.FromResult(_result.ToArray());
        }

        private async Task HandleInternal(string srcDir)
        {
            try
            {
                var result = await Action.DoActionAsync(srcDir);
                _result.Add(result);
            }
            catch (UnauthorizedAccessException)
            {
                Log.Error($"Access denied to:{srcDir}");
                return;
            }
            catch (Exception e)
            {
                Log.Fatal("Unknown error", e);
                Environment.FailFast("Unknown error occurred", e);
            }

            foreach (var dir in Directory.GetDirectories(srcDir))
            {
               await HandleInternal(dir);
            }
        }
    }
}