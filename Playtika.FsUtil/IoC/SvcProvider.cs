﻿using System;
using Autofac;

namespace Playtika.FsUtil
{
    public class SvcProvider
    {
        private static IContainer Build()
        {
            var builder = new ContainerBuilder();

            builder.RegisterType<CommandHandler>().InstancePerDependency();

            builder.RegisterType<AllCommandAction>().Named<ICommandAction>("all");
            builder.RegisterType<CppCommandAction>().Named<ICommandAction>("cpp");
            builder.RegisterType<Reversed1CommandAction>().Named<ICommandAction>("reversed1");
            builder.RegisterType<Reversed2CommandAction>().Named<ICommandAction>("reversed2");

            return builder.Build();
        }
        private static readonly Lazy<IContainer> InstanceContainer = new Lazy<IContainer>(Build);
        public static IContainer Instance => InstanceContainer.Value;
    }
}