﻿using System.IO;
using System.Linq;

namespace Playtika.FsUtil
{
    public sealed class AllCommandAction : CommandActionBase
    {
        public AllCommandAction(ICommandActionSource actionSource) : base(actionSource)
        {
        }

        public override string[] DoAction(string dirFilepath)
        {
            return Directory.GetFiles(dirFilepath)
                            .Select(MakeRelative)
                            .ToArray();
        }
    }
}