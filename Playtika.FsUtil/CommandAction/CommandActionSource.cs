﻿namespace Playtika.FsUtil
{
    public sealed class CommandActionSource : ICommandActionSource
    {
        public string SourceDir { get; }
        public string CommandAction { get; }
        public string OutputFilepath { get; }

        public CommandActionSource(string sourceDir, string commandAction, string outputFilepath)
        {
            SourceDir = sourceDir;
            CommandAction = commandAction ?? string.Empty;
            OutputFilepath = outputFilepath;
        }
    }
}