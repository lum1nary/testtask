﻿using System.IO;
using System.Linq;

namespace Playtika.FsUtil
{
    public sealed class CppCommandAction : CommandActionBase
    {
        public CppCommandAction(ICommandActionSource actionSource) : base(actionSource)
        {
        }

        public override string[] DoAction(string dirFilepath)
        {
            return Directory.GetFiles(dirFilepath, "*.cpp")
                            .Select(i => MakeRelative(i) + "/")
                            .ToArray();
        }
    }
}