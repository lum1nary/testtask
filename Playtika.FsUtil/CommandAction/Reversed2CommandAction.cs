﻿using System.IO;
using System.Linq;

namespace Playtika.FsUtil
{
    public sealed class Reversed2CommandAction : CommandActionBase
    {
        public Reversed2CommandAction(ICommandActionSource actionSource) : base(actionSource)
        {
        }

        public override string[] DoAction(string dirFilepath)
        {
            return Directory.GetFiles(dirFilepath)
                            .Select(MakeRelative)
                            .Select(i => new string(i.Reverse().ToArray()))
                            .ToArray();
        }
    }
}