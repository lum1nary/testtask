﻿using System.IO;
using System.Linq;

namespace Playtika.FsUtil
{
    public class Reversed1CommandAction : CommandActionBase
    {
        public Reversed1CommandAction(ICommandActionSource actionSource) : base(actionSource)
        {
        }

        public override string[] DoAction(string dirFilepath)
        {
            return Directory.GetFiles(dirFilepath)
                            .Select(MakeRelative)
                            .Select(ReversePath)
                            .ToArray();
        }

        private string ReversePath(string sourcePath)
        {
            return string.Join("\\", sourcePath.Split('\\').Reverse());
        }
    }
}