﻿using System;
using System.Threading.Tasks;

namespace Playtika.FsUtil
{
    public abstract class CommandActionBase : ICommandAction
    {
        public  ICommandActionSource ActionSource { get; }

        protected string MakeRelative(string targetDir)
        {
            return targetDir.Replace(ActionSource.SourceDir, string.Empty);
        }

        protected CommandActionBase(ICommandActionSource source)
        {
            if(source == null)
                throw new ArgumentNullException();

            ActionSource = source;
        }

        public abstract string[] DoAction(string dirFilepath);

        public async Task<string[]> DoActionAsync(string dirFilepath)
        {
            return await Task.Run(() => DoAction(dirFilepath));
        }
    }
}