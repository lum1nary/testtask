﻿using System.Threading.Tasks;

namespace Playtika.FsUtil
{
    public interface ICommandAction
    {
        ICommandActionSource ActionSource { get; }
        string[] DoAction(string dirFilepath);
        Task<string[]> DoActionAsync(string dirFilepath);

    }
}