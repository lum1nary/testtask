﻿namespace Playtika.FsUtil
{
    public interface ICommandActionSource
    {
        string SourceDir { get; }
        string CommandAction { get; }
        string OutputFilepath { get; }
    }
}