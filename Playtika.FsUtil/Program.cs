﻿using System;
using System.Configuration;
using System.IO;
using System.Linq;
using Autofac;
using log4net;

namespace Playtika.FsUtil
{
    class Program
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(Program));

        static void Main(string[] args)
        {
            try
            {
                Validation.ValidateInput(args);
            }
            catch (Exception e)
            {
                Log.Error(e.Message, e);
                Environment.Exit(1);
            }

            string outputDir;
            try
            {
                outputDir = args[2];
            }
            catch (IndexOutOfRangeException)
            {
                outputDir = ConfigurationManager.AppSettings["defaultOutput"];
            }

            var actionSource = new CommandActionSource(args[0], args[1], outputDir);
            CommandHandler handler;
            try
            {
                var actionParam = new TypedParameter(typeof(ICommandActionSource), actionSource);
                handler = SvcProvider.Instance.Resolve<CommandHandler>(actionParam);
            }
            catch (Exception)
            {
                return;
            }
            
            var resultTask =  handler.Handle();
            Console.WriteLine("Working...");

            var writeTask = resultTask.ContinueWith(t =>
            {
                var strResult = t.Result.SelectMany(i => i).ToArray();
                try
                {
                    File.WriteAllLines(outputDir, strResult);
                }
                catch (Exception e)
                {
                    Log.Error("Failed to write results", e);
                    Environment.Exit(1);
                }
            });

            writeTask.Wait();
            Log.Info("Work done! Result file location:" + outputDir);
        }
    }
}