﻿using System;
using System.IO;

namespace Playtika.FsUtil
{
    public class Validation
    {
        public static void ValidateInput(string[] input)
        {
            if (input == null)
                throw new ArgumentNullException(nameof(input));

            if (input.Length < 2 || input.Length > 3)
                throw new InvalidOperationException("Incorred number of arguments");

            if (!Directory.Exists(input[0]))
                throw new DirectoryNotFoundException("source directory is not exists");

            if(string.IsNullOrEmpty(input[1]))
                throw new ArgumentException("command is empty");
        }
    }
}