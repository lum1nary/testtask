﻿using System;
using Autofac;
using Autofac.Core.Registration;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Playtika.FsUtil.Tests
{
    [TestClass]
    public class IoCTests
    {
        private const string AllComand = "all";
        private const string CppCommand = "cpp";
        private const string ReverseOneCommand = "reversed1";
        private const string ReverseTwoCommand = "reversed2";

        [TestMethod]
        public void AllCommandActionTest()
        {
            var actionSource = new CommandActionSource(
                                            Environment.CurrentDirectory, 
                                            AllComand,
                                            string.Empty);

            CommandActionTest(actionSource, typeof(AllCommandAction));
        }

        [TestMethod]
        public void CppCommandActionTest()
        {
            var actionSource = new CommandActionSource(
                Environment.CurrentDirectory,
                CppCommand,
                string.Empty);

            CommandActionTest(actionSource, typeof(CppCommandAction));
        }

        [TestMethod]
        public void ReverseOneCommandActionTest()
        {
            var actionSource = new CommandActionSource(
                Environment.CurrentDirectory,
                ReverseOneCommand,
                string.Empty);
            
            CommandActionTest(actionSource, typeof(Reversed1CommandAction));
        }

        [TestMethod]
        public void ReverseTwoCommandActionTest()
        {
            var actionSource = new CommandActionSource(
                Environment.CurrentDirectory,
                ReverseTwoCommand,
                string.Empty);

            CommandActionTest(actionSource, typeof(Reversed2CommandAction));
        }

        private void CommandActionTest(ICommandActionSource actionSource, Type expectedType)
        {
            var actionParam = new TypedParameter(typeof(ICommandActionSource), actionSource);
            var action = SvcProvider.Instance.ResolveNamed<ICommandAction>(actionSource.CommandAction, actionParam);

            Assert.IsNotNull(action);
            Assert.IsInstanceOfType(action, expectedType);
        }

        [TestMethod]
        [ExpectedException(typeof(ComponentNotRegisteredException))]
        public void UnknownCommandTest()
        {
            var actionSource = new CommandActionSource(
                Environment.CurrentDirectory,
                "unknown",
                string.Empty);
            
            CommandActionTest(actionSource, null);
        }
        
    }
}
