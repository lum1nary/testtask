﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Playtika.FsUtil.Tests
{
    [TestClass]
    public class ValidationTests
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void NullArrayTest()
        {
            string[] input = null;
            Validation.ValidateInput(input);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void EmptyArrayTest()
        {
            string[] input = new string[0];
            Validation.ValidateInput(input);
        }

        [TestMethod]
        [ExpectedException(typeof(DirectoryNotFoundException))]
        public void NonExistSourceDirTest()
        {
            string[] input = new string[2] {"C:\\NotExist", "all"};
            Validation.ValidateInput(input);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void CommandEmptyTest()
        {
            string[] input = new string[2] {"C:\\Program Files", null};
            Validation.ValidateInput(input);
        }

        [TestMethod]
        public void CorrectValidationTest()
        {
            string[] input = new string[2] {"C:\\Program Files\\", "all"};
            Validation.ValidateInput(input);
        }
    }
}