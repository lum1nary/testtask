﻿using System;
using System.IO;
using System.Linq;
using Autofac;
using Autofac.Core;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Playtika.FsUtil.Tests
{
    [TestClass]
    public class CommandHandlerTests
    {
        [TestMethod]
        public void AllCommandHandleTest()
        {
            var resultPath = "myresult.log";

            var commandSource = new CommandActionSource("E:\\Movies\\", "all", resultPath);
            var handlerParam = new TypedParameter(typeof(ICommandActionSource), commandSource);
            var handler = SvcProvider.Instance.Resolve<CommandHandler>(handlerParam);
            
            var handleTask = handler.Handle();
            handleTask.Wait();
            
            Assert.IsTrue(handleTask.IsCompleted);
            Assert.IsNotNull(handleTask.Result);
            Assert.IsTrue(handleTask.Result.Length > 0);
            File.WriteAllLines(handler.Action.ActionSource.OutputFilepath,handleTask.Result.SelectMany(i => i));
            //in this case we should check file content/task result for equality to...
            Assert.IsTrue(File.Exists(Path.Combine(Environment.CurrentDirectory,resultPath)));
        }

        [TestMethod]
        //Because autofac wrapps Component not registered ex into dependencyResolve ex
        [ExpectedException(typeof(DependencyResolutionException))]
        public void UnknownCommandHandleTest()
        {
            var resultPath = "myresult.log";

            var commandSource = new CommandActionSource("E:\\Movies\\", "unknown", resultPath);
            var handlerParam = new TypedParameter(typeof(ICommandActionSource), commandSource);
            var handler = SvcProvider.Instance.Resolve<CommandHandler>(handlerParam);
        }
    }
}